Name:           puppet-alternatives
Version:        1.0
Release:        3%{?dist}
Summary:        puppet alternatives module

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz
BuildArch:      noarch
Requires:       puppet-agent

%description
puppet  alternatives module.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/alternatives/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/alternatives/
touch %{buildroot}/%{_datadir}/puppet/modules/alternatives/supporting_module

%files -n puppet-alternatives
%{_datadir}/puppet/modules/alternatives

%changelog
* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-3
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-2
- fix requires on puppet-agent

* Mon Mar 02 2020 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
- initial release for c8
